﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Utils;

namespace App
{
    internal static class Program
    {
        [ExcludeFromCodeCoverage]
        private static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                throw new Exception($"Application should take 3 parameters (not {args.Length})");
            }

            var arg1 = int.Parse(args[0], CultureInfo.InvariantCulture);
            var operation = char.Parse(args[1]);
            var arg2 = int.Parse(args[2], CultureInfo.InvariantCulture);
            var result = 0;

            var calculator = new Calculator(arg1, arg2);

            result = operation switch
            {
                '+' => calculator.Add(),
                '-' => calculator.Sub(),
                '*' => calculator.Mul(),
                '/' => calculator.Div(),
                _ => throw new Exception($"Invalid operation'{operation}'")
            };

            Console.WriteLine($"{arg1} {operation} {arg2} = {result}");
        }
    }
}