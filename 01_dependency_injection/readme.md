# Dependency Injection

Dependency container allow us to avoid writing code for dependency management.
Container creates or stores all objects and automatically passes them where needed.
Most basic way of injection is through constructor parameters - this in done in this lesson examples.

```C#
interface IDummy {}
class Dummy : IDummy {}

class Example
{
    private readonly IDummy _dummy;

    public Example(IDummy dummy)
    {
        _dummy = dummy;
    }
}
```
Concrete implementation instance will be created by container and returned as interface type.

There are also other ways such as property injection or method injection - hover this might require
more advanced container implementation than one provided by framework.

There are some components that are compatible with container.
We can add logging package to **Utils** and concrete implementation in **App**:

```bash
cd Utils/
dotnet add package Microsoft.Extensions.Logging --version 3.0.0-preview8.19405.4
```

````bash
cd App/
dotnet add package Microsoft.Extensions.Logging.Console --version 3.0.0-preview8.19405.4
````