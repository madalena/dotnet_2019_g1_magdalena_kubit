using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class DelayedPrintCommand : Command
    {
        public override Task<string> RunAsync(CancellationToken cancellationToken)
        {
            Task<string> task1 = new Task<string>(() =>
            {
                //Task.Delay(this.Delay);
                _time.Delay(Delay, cancellationToken);
                return this.Message;
            }, cancellationToken);
            task1.Start();

            return task1;
        }

        public DelayedPrintCommand(string message, int delay, ITime time)
        {
            _time = time;
            Message = message;
            Delay = delay;
            Name = "delayed print '" + this.Message + "' in " + this.Delay + " ms";

        }

        public int Delay { get; }
        public string Message { get; }
        private readonly ITime _time;
        public override string Name { get; }
    }
}