using System;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class NullCommand : Command
    {
        public NullCommand()
        {
            this.Name = "null";
        }
        public override Task<string> RunAsync(CancellationToken cancellationToken)
        {
            Task<string> task1 = new Task<string>(() => "null", cancellationToken);
            task1.Start();
            return task1;
        }
        
        public override string Name { get; }
    }
}