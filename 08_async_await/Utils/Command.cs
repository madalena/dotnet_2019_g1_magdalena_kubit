using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public abstract class Command
    {
        /*public Command(string[] arguments) //abstract?
        {
            //throw new System.NotImplementedException();
            // zgodnie z argumentami tworzymy odpowiednią komendę -> może to byc
            if (arguments != null)
            {
                Console.WriteLine(arguments[0] ?? "NULL");
            }
            Console.WriteLine("ELO");
        }*/
        
        public virtual string Name { get;  }

        public Command()
        {
            this.Name = "";
        }
        public abstract Task<string> RunAsync(CancellationToken cancellationToken);

        public virtual IEnumerable<Command> Continuations(string result, ICommandFactory commandFactory)
        {
            return new List<Command>();
        }
    }
}