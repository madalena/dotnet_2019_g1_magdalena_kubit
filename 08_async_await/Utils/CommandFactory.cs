using System.Threading;

namespace Utils
{
    public class CommandFactory : ICommandFactory
    {
        public CancellationTokenSource CancellationTokenSource { get; }

        public CommandFactory(CancellationTokenSource source)
        {
            this.CancellationTokenSource = source;
        }
        public Command Create(params string[] arguments)
        {
            //Command command = Create(arguments);
            //return command;
            return new NullCommand();
        }
    }
}