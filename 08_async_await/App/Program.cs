﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Utils;

namespace App
{
    internal static class Program
    {
        [ExcludeFromCodeCoverage]
        private static async Task Main()
        {
            using var cancellationTokenSource = new CancellationTokenSource();
            var commandFactory = new CommandFactory(cancellationTokenSource);
            var engine = new Engine(commandFactory);
            await engine.RunAsync();
        } 
    }
}

/*
 * Notatki:
 * Programowanie asynchroniczne a równoległe
*/
internal static class Program
{
    private static async Task<int> Main(string[] args)
    {
        await Task.Delay(2000);
        Console.WriteLine("dsadada");
        await Task.Delay(2000);

        return 0;
    }
}