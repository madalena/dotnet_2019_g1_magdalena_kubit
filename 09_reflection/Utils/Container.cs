using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Utils
{
    public class Container : IContainer
    {
        private class TypeWithBehaviour
        {
            public TypeWithBehaviour(Type type, bool singleton)
            {
                Type = type;
                Singleton = singleton;
            }

            public Type Type { get; }
            public bool Singleton { get; }
        }

        private readonly Dictionary<Type, Dictionary<string, TypeWithBehaviour>> _mapping =
            new Dictionary<Type, Dictionary<string, TypeWithBehaviour>>();

        private readonly Dictionary<Type, Dictionary<string, object>> _singletons =
            new Dictionary<Type, Dictionary<string, object>>();

        public void Map(Type from, Type into, string name = "", bool singleton = false)
        {
            if (from == null) throw new ArgumentNullException(nameof(from));
            if (into == null) throw new ArgumentNullException(nameof(into));

            if (!into.IsSubclassOf(from))
                throw new Exception($"Cannot map from {from.Name} into {into.Name}!");

            if (!_mapping.ContainsKey(from))
                _mapping[from] = new Dictionary<string, TypeWithBehaviour>();

            _mapping[from][name] = new TypeWithBehaviour(into, singleton);
        }

        public void Map<TFrom, TInto>(string name = "", bool singleton = false) where TFrom : class where TInto : TFrom
        {
            Map(typeof(TFrom), typeof(TInto), name, singleton);
        }

        public object? Create(Type? type, string name = "")
        {
            // TODO: Throw when type is null
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            // TODO: Find mapped type and whether it is a singleton or not
            if (_mapping.ContainsKey(type) || _singletons.ContainsKey(type))
            {
                //if _singletons.TryGetValue(type, );
                //var constructors = type.GetConstructors();
                //foreach (var constructor in constructors)
                //{
                    //var parameters = constructor.GetParameters();
                    //foreach (var parameter in parameters)
                    //{
                        //zrob mapowanie
                    //}
                    //zrob mapowanie
                    //var newType = _mapping[type];
                    //var newObject = newType[name].Type;
                    //return newObject;
               // }

                var x = _mapping[type];
                
                if (x.ContainsKey(name))
                {
                    var y = x[name];
                    var mappedType = y.Type;
                    var constructors = mappedType.GetConstructors();
                    // po parametrach trzeba jeszcze przejść

                    //if (constructors.Length == 0)
                    //{
                    //foreach (var constructor in constructors)
                    //{
                    var constructor = type
                        .GetConstructors().FirstOrDefault();
                        //.FirstOrDefault//(c => c.GetParameters().Length > 0);
                    
                    var parameters = constructor?.GetParameters();/*.Select(p =>
                            p.HasDefaultValue
                                ? p.DefaultValue
                                : p.ParameterType.IsValueType && Nullable.GetUnderlyingType(p.ParameterType) == null
                                    ? Activator.CreateInstance(p.ParameterType)
                                    : null
                        );*/
                        
                        //var paramList = new List<Type>();
                        //var myObj; //= constructor.Invoke(Array.Empty<object>());
                        //var parametersTable[] = new Array[object];
                        //var parametersTable = new object?[parameters.Length];
                        /*int i = 0;
                        foreach (var parameter in parameters)
                        {

                            //paramList.Add(Create(parameter.ParameterType, ""));
                            //paramList.Add(parameter.ParameterType.GetConstructors()[0].Invoke(Array.Empty<object>()).GetType());
                            var par = Create(parameter);
                            if (par != null)
                            {
                                parametersTable[i] = par;
                            }
                            //parametersTable[i] = Create(parameter.ParameterType);
                            //i++;
                            //
                        //}*/

                        var par = Create(parameters?[0].GetType());
                        var myObj = constructor?.Invoke(parameters);//Array.Empty<object>());

                        return myObj;

                        //if (paramList.Count == 0)
                        //{
                        //var myObj2 = constructor.Invoke(Array.Empty<object>());//;
                        //return myObj2;
                        //}
                        
                        //var myObj2 = constructor.Invoke(new object[] {paramList});
                        //return myObj2;

                    //}
                    
                    //throw new Exception("Cannot find constructor!");
                }
                
                //Type myType = ;
                //Type[] types = new Type[1];
                //types[0] = typeof(int);

                //var myObj = constructors[0].Invoke();
                //return _mapping[type][name].Type;
            }


            // TODO: If it is a singleton and already created then return it ??

            // TODO: Get type constructor

            // TODO: Get constructor parameters

            // TODO: Recursively create constructor parameters - call or every one Create(...)
            
            // TODO: Invoke constructor with created parameters

            // TODO: Save object if it is a singleton 

            // TODO: Return object

            //return _mapping[type][name].Type;
            
            // znależź mapowane w _mapping czy type jest to idziemy dalej, jeżeli nie, to wyjątek
            // wyciągamy używając refleksji konstruktor, wyciągamy argumenty, potem dla każdego agumentu rekurencyjnie tworzymy Create()
            return null;
        }

        public T? Create<T>(string name = "") where T : class
        {
            return Create(typeof(T), name) as T;
        }
    }
}

/*
 * paczka, która trzyma różnego rodzaju obikty i umozliwia wyciągnięcie jakiegoś obiektu, np obiekt sesji
 * zaimpl kontenera
*/

//if _mapping.ContainsKey(type))
//var x = _mapping[type];