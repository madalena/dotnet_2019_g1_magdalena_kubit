using Xunit;

namespace Test
{
    public class RangesTest
    {
        [Fact]
        public void GettingVariousRanges()
        {
            var data = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            {
                var result = data[1..9];

                var expected = new[] {1, 2, 3, 4, 5, 6, 7, 8};
                Assert.Equal(expected, result);
            }

            {
                var result = data[^2..];

                var expected = new[] {8, 9};
                Assert.Equal(expected, result);
            }

            {
                var result = data[1..5];

                var expected = new[] {1, 2, 3, 4};
                Assert.Equal(expected, result);
            }
        }
    }
}