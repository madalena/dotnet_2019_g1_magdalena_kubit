using System;
using System.Collections.Generic;
using Utils;
using Xunit;

namespace Test
{
    public class CollectionExtensionsTest
    {
        [Fact]
        public void AvgStdDevCanHandleNull()
        {
            IEnumerable<int> enumerable = null!;
            Assert.Throws<ArgumentNullException>(() => enumerable.AvgStdDev());
        }

        [Fact]
        public void AvgStdDevForConstantData()
        {
            var data = new[] {1, 1, 1, 1, 1};

            Assert.Equal((avg: 1, stdDev: 0), data.AvgStdDev());
        }

        [Fact]
        public void AvgStdDevForPlusMinusOneDeviation()
        {
            var data = new[] {0, -1, 0, 1, 0};

            var (avg, stdDev) = data.AvgStdDev();

            Assert.Equal(0, avg, 0);
            Assert.Equal(Math.Sqrt(2.0 / 5.0), stdDev, 2);
        }

        [Fact]
        public void AvgStdDevForSomeExampleData()
        {
            var data = new[] {6, 2, 3, 1};

            var (avg, stdDev) = data.AvgStdDev();

            Assert.Equal(3, avg, 0);
            Assert.Equal(Math.Sqrt(14.0 / 4.0), stdDev, 2);
        }

        [Fact]
        public void MinMaxCanHandleNull()
        {
            IEnumerable<int> enumerable = null!;
            Assert.Throws<ArgumentNullException>(() => enumerable.MinMax());
        }

        [Fact]
        public void MinMaxForEmptyCollection()
        {
            var data = Array.Empty<int>();

            Assert.Equal((min: int.MaxValue, max: int.MinValue), data.MinMax());
        }

        [Fact]
        public void MinMaxForOneElement()
        {
            var data = new[] {1};

            Assert.Equal((min: 1, max: 1), data.MinMax());
        }

        [Fact]
        public void MinMaxForOneMultipleElements()
        {
            var data = new[] {-4, -2, 0, 2, 4};

            Assert.Equal((min: -4, max: 4), data.MinMax());
        }
    }
}