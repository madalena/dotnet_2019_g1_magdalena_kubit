using Utils;
using Xunit;

namespace Test
{
    public class PatternsTest
    {
        private class UnknownToMatcher
        {
        }

        [Fact]
        public void EmptyString()
        {
            Assert.Equal(Result.EmptyString, Patterns.Match(""));
        }

        [Fact]
        public void Null()
        {
            Assert.Equal(Result.Null, Patterns.Match(null!));
        }

        [Fact]
        public void StringWithExactlyFiveCharacters()
        {
            Assert.Equal(Result.FiveCharacterString, Patterns.Match("12345"));
        }

        [Fact]
        public void StringWithLessThanFiveCharacters()
        {
            Assert.Equal(Result.ShortString, Patterns.Match("123"));
        }

        [Fact]
        public void StringWithMoreThanFiveCharacters()
        {
            Assert.Equal(Result.LongString, Patterns.Match("1234567"));
        }

        [Fact]
        public void TuplePoint2D()
        {
            Assert.Equal(Result.Point2D, Patterns.Match((11, 20)));
        }

        [Fact]
        public void TuplePoint2DOrigin()
        {
            Assert.Equal(Result.Point2DOrigin, Patterns.Match((0, 0)));
        }

        [Fact]
        public void TuplePoint2DSumOfCoordinatesIsEven()
        {
            Assert.Equal(Result.Point2DSumOfCoordinatesIsEven, Patterns.Match((11, 21)));
        }

        [Fact]
        public void Unknown()
        {
            var unknownToMatcher = new UnknownToMatcher();
            Assert.Equal(Result.Unknown, Patterns.Match(unknownToMatcher));
        }
    }
}