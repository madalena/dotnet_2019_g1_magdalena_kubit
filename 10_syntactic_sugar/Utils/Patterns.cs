using System;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Schema;

namespace Utils
{
    public enum Result
    {
        Null,
        EmptyString,
        ShortString,
        FiveCharacterString,
        LongString,
        Point2D,
        Point2DOrigin,
        Point2DSumOfCoordinatesIsEven,
        Unknown
    }

    public static class Patterns
    {
        public static Result Match(object o)
        {
            return o switch
            {
                "" => Result.EmptyString,
                string s when s.Length == 5 => Result.FiveCharacterString,
                string s when s.Length < 5 => Result.ShortString,
                string s when s.Length > 5 => Result.LongString,
                (0,0) => Result.Point2DOrigin,
                (int x, int y) when (x+y)%2==0 => Result.Point2DSumOfCoordinatesIsEven,
                (_, _) => Result.Point2D,
                null => Result.Null,
                _ => Result.Unknown
            };
        }
    }
}