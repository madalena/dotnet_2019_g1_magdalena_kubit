﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Utils
{
    public static class CollectionExtensions
    {
        // TODO: MinMax ...

        // TODO: AvgStdDev ...
        public static (double avg, double stdDev) AvgStdDev(this IEnumerable<int> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var avg = data.Average();
            //return (data.Average(), data.
            //
            //double avg = values.Average();
            //return Math.Sqrt(values.Average(v=>Math.Pow(v-avg,2)));
            return (avg, Math.Sqrt(data.Average(v => Math.Pow(v - avg, 2))));

        }

        public static (int min, int max) MinMax(this IEnumerable<int> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            return (data.Min(), data.Max());
        }
    }
}