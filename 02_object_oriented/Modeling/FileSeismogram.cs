using System;
using System.Globalization;
using System.IO;

namespace Modeling
{
    public class FileSeismogram : ISeismogram
    {
        private readonly TextWriter _textWriter;

        public FileSeismogram(TextWriter textWriter)
        {
            _textWriter = textWriter;
        }

        public void Store(double time, double value)
        {
            var format = CultureInfo.InvariantCulture;
            FormattableString formattableString = $"{time} {value}";

            _textWriter.WriteLine(formattableString.ToString(format));
        }

        public void Close()
        {
            _textWriter.Close();
        }
    }
}