using static System.Math;

namespace Modeling
{
    public class SinSource : ISource
    {
        public SinSource(double frequency, double phase, double amplitude)
        {
            Frequency = frequency;
            Phase = phase;
            Amplitude = amplitude;
        }

        public double Amplitude { get; }
        public double Frequency { get; }
        public double Phase { get; }

        public double Sample(double time)
        {
            return Amplitude * Sin(2.0 * PI * Frequency * time + Phase);
        }
    }
}