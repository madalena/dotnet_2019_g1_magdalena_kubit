using System.Collections.Generic;

namespace Utils
{
    public class RangeFibonacci : Fibonacci
    {
        private readonly int _start;
        private readonly int _step;
        private readonly int _stop;

        public RangeFibonacci(int start = 0, int step = 1, int stop = 1)
        {
            _start = start;
            _step = step;
            _stop = stop;
        }

        public override IEnumerable<int> Numbers()
        {
            var i = 0;
            foreach (var number in base.Numbers())
            {
                var n = i++;

                if (n > _stop) break;
                if (n < _start || n > _stop) continue;

                if ((n - _start) % _step == 0)
                    yield return number;
            }
        }
    }
}