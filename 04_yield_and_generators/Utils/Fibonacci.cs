using System;
using System.Collections.Generic;

namespace Utils
{
    public class Fibonacci
    {
        public virtual IEnumerable<int> Numbers()
        {
            var current = 0;
            var next = 1;

            for (var n = 0; n <= 43; n++)
            {
                yield return current;

                var oldCurrent = current;
                current = next;
                next += oldCurrent;
            }

            throw new IndexOutOfRangeException("Cannot really go further for signed 32bit value :)");
        }
    }
}