using System;
using System.Transactions;

namespace Utils
{
    public class LoggingButton : Button
    {
        public override void OnClicked()
        {
            base.OnClicked();
            Console.WriteLine("Clicked " + Label);
        }

        public LoggingButton(string label) : base(label)
        {
            this.Label = label;
        }
    }
}