namespace Utils
{
    public partial class Window
    {
        public bool HandledButtonOkClick { get; private set;}
        public bool HandledButtonCancelClick { get; private set; }

        void ButtonOk_Clicked(object sender, ClickedEventArgs e)
        {
            HandledButtonOkClick = true;
        }
        
        void ButtonCancel_Clicked(object sender, ClickedEventArgs e)
        {
            HandledButtonCancelClick = true;
        }

        public void SimulateClick()
        {
            buttonOk?.Click();
            buttonCancel?.Click();
        }

        public Window()
        {
            InitializeComponent();
        }

        public LoggingButton? buttonOk { get; set; }
        public LoggingButton? buttonCancel { get; set; }

        private void InitializeComponent()
        {
            buttonOk = new LoggingButton("OK");
            buttonCancel = new LoggingButton("Cancel");
            buttonOk.Clicked += ButtonOk_Clicked;
            buttonCancel.Clicked += ButtonCancel_Clicked;
        }
    }
}