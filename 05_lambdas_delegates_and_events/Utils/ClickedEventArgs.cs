using System;

namespace Utils
{
    public class ClickedEventArgs : EventArgs
    {
        public string? Label { get; set; }
    }
}