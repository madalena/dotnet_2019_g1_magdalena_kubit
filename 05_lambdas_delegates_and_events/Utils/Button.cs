using System;

namespace Utils
{
    public class Button
    {
        public string Label { get; set; }

        public event EventHandler<ClickedEventArgs>? Clicked;

        protected Button(string label)
        {
            this.Label = label;
        }
        
        public void Click()
        {
            Console.WriteLine("In click -> gonna click " + Label);
            OnClicked();
        }

        public virtual void OnClicked()
        {
            var e = new ClickedEventArgs {Label = this.Label};
            if (Clicked == null) return;
            
            var handler = Clicked;
            handler(this, e);
        }
    }

}