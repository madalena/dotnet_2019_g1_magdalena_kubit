using Utils;
using Xunit;

namespace Test
{
    public class WindowTest
    {
        [Fact]
        public void ButtonOnClickSetsHandledButtonsOnTrue()
        {
            Window window = new Window();
            window.SimulateClick();
            
            Assert.True(window.HandledButtonOkClick);
            Assert.True(window.HandledButtonCancelClick);
        }

        [Fact]
        public void ButtonsAreInitializedProperly()
        {
            Window window = new Window();
            window.SimulateClick();
            
            Assert.Equal("OK", window.buttonOk?.Label);
            Assert.Equal("Cancel", window.buttonCancel?.Label);
        }

        [Theory]
        [InlineData("OK")]
        [InlineData("Cancel")]
        public void ClickedEventArgsReturnsProperLabel(string label)
        {
            ClickedEventArgs args = new ClickedEventArgs {Label = label};

            Assert.Equal(label, args.Label);
        }

        [Theory]
        [InlineData("OK")]
        [InlineData("Cancel")]
        public void OnClickedSetsEventArgsLabel(string label)
        {
            Button button = new LoggingButton(label);
            button.OnClicked();
            
            Assert.Equal(label, button.Label);
        }
    }
}