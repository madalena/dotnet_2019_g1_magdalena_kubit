﻿using System.Diagnostics.CodeAnalysis;
using Utils;

namespace App
{
    internal static class Program
    {
        [ExcludeFromCodeCoverage]
        private static void Main()
        {
            Window window = new Window();
            window.SimulateClick();
        }
    }
}