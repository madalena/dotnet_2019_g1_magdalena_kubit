# Multi-threading

To add benchmark dependencies execute:

```bash
dotnet add package xunit.performance.api --version 1.0.0-beta-build0020
```

Command was taken from [this](https://dotnet.myget.org/feed/dotnet-core/package/nuget/xunit.performance.api) page.

Please not that this package is not present in [nuget.org](nuget.org) repository. 
Separate source from [myget.org](myget.org) was added in **NuGet.Config**:
```xml
<add key="dotnet.myget.org" value="https://dotnet.myget.org/F/dotnet-core/api/v3/index.json" />
```
