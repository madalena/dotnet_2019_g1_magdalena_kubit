﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class Demo
    {
        private readonly int[] _data;

        public Demo(int[] data)
        {
            _data = data;
        }

        public int Sum()
        {
            var sum = 0;
            for (int i = 0; i < _data.Length; i++)
            {
                sum += _data[i];
            }
            
            return sum;
        }

        public int SumForeach()
        {
            // TODO: Use foreach loop to calculate sum
            var sum = 0;
            foreach (var data in _data)
            {
                sum += data;
            }
            
            return sum;
        }

        public int SumLinq()
        {
            // TODO: Use for LINQ to calculate sum
            var sum = (from data in _data  select data).Sum();
            
            return sum;
        }

        private delegate void ThreadAction(int start, int stop);

        private void RunStandaloneThreads(int count, ThreadAction action)
        {
            // TODO: Run 'count' threads and execute 'action' in every one with appropriate range of data
            if (count >= _data.Length)
                count = _data.Length;

            var threads = new Thread[count];
            for (int i = 0; i < count; i++)
            {
                var start = i * (_data.Length / count);//_data.Length / count + i * (_data.Length / count);
                var stop = 0;
                if (i == count - 1)
                    stop = _data.Length;
                else
                {
                    stop = _data.Length / count + i * (_data.Length / count);
                }
                threads[i] = new Thread(() => action(start, stop));
                threads[i].Start();
            }
            
            for(int i= 0; i<count; i++)
                threads[i].Join();
        }

        public int SumThreadsInterlocked(int count)
        {
            // TODO: Use 'Interlocked.Add' to calculate total sum
            // TODO: Use 'RunStandaloneThreads' to run threads
            // TODO: Ue lambda to poss 'action' to 'RunStandaloneThreads'

            var sum = 0;
            RunStandaloneThreads(count, (start, stop) =>
            {
                for (var i = start; i < stop; i++)
                {
                    //sum += _data[i]; //musi być jakoś kontrolowane, bo to dodawanie z różnych wątków
                    Interlocked.Add(ref sum, _data[i]);
                }

            });
            
            return sum;
        }


        public int SumThreads(int count)
        {
            // TODO: Use 'Interlocked.Add' to calculate total sum
            // TODO: Use partial sum in threads to avoid excessive use of 'Interlocked.Add'
            // TODO: Use 'RunStandaloneThreads' to run threads
            // TODO: Ue lambda to poss 'action' to 'RunStandaloneThreads'
            var sum = 0;
            RunStandaloneThreads(count, (start, stop) =>
            {
                var sum2 = 0;
                for (var i = start; i < stop; i++)
                {
                    //sum += _data[i]; //musi być jakoś kontrolowane, bo to dodawanie z różnych wątków
                    //Interlocked.Add(ref sum, _data[i]);
                    sum2 += _data[i];
                }
            
                Interlocked.Add(ref sum, sum2);
            });

            
            return sum;
        }

        private void RunPoolThreads(int count, ThreadAction action)
        {
            // TODO: Run 'count' pool threads and execute 'action' in every one with appropriate range of data
            // TODO: Use 'ManualResetEvent' to synchronize operations
            /*var sum = 0;
            for (int i = 0; i <= 10; i++)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(RunStandaloneThreads(count, (start, stop) =>
                {
                    for (var i = start; i < stop; i++)
                    {
                        //sum += _data[i]; //musi być jakoś kontrolowane, bo to dodawanie z różnych wątków
                        //Interlocked.Add(ref sum, _data[i]);
                        Interlocked.Add(ref sum, _data[i]);
                    }
                })));
            }
            //ManualResetEvent MRE = (ManualResetEvent) action;
            //MRE.Set();
            
            WaitCallback CallbackMethod = new WaitCallback(MRE);
            WaitHandle [ ]WaitHandleArray = new WaitHandle[1];
            WaitHandleArray[0] = new ManualResetEvent(false);
            ThreadPool.QueueUserWorkItem(CallbackMethod, WaitHandleArray[0]);
            WaitHandle.WaitAll(WaitHandleArray);
            
            ManualResetEvent mre = new ManualResetEvent(false);
            
            WaitCallback CallbackMethod = new WaitCallback( (state =>
            {
                
            }));
            ThreadPool.QueueUserWorkItem(CallbackMethod, count);*/

        }

        public int SumPoolThreads(int count)
        {
            // TODO: Use 'RunPoolThreads' to run threads
            // TODO: Use 'Interlocked.Add' to aggregate data
            // TODO: Ue lambda to poss 'action' to 'RunPoolThreads'
            
            var sum = 0;
            //WaitCallback CallbackMethod = new WaitCallback());
            //ThreadPool.QueueUserWorkItem(CallbackMethod, count);
            
            
            
            return sum;
        }

        /*public int SumTpl()
        {
            // TODO: Use 'Parallel.For' to calculate sum
            // TODO: Ue lambdas to poss operations

            return 0;
        }*/
    }
}